# culane_view

View the CuLane dataset, the images and lines.

# Setup
Install Python 3.8. It may work with older versions of Python 3, I haven't tested. The exact version
I am using is Python 3.8.6.

## Install the requirements
You need `opencv` and `numpy`, you can install with the following command as well:
```
    pip install -r requirements.txt
```
Using the command above should work the best as it would be the same versions as I used.

# Downloading the dataset
The dataset is not included, and it is available on its own license on the official [CuLane website](https://xingangpan.github.io/projects/CULane.html).
