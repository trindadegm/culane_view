import argparse
import os
from os import path

import cv2


FRAME_DELAY_MS = 1000//30


def main(options):
    input_dir = options.input_dir
    dir_list = list(filter(
        path.isdir,
        map(lambda d: path.join(input_dir, d), os.listdir(input_dir))
    ))
    dir_list.sort()

    frames = gen_frames(dir_list)

    print("Pess 'q' to quit (on the window).")

    for (img, lines) in frames:
        new_h = img.shape[0]//3
        new_w = img.shape[1]//3
        img = cv2.resize(img, (new_w, new_h))

        for line in lines:
            # The 1/4 is the scale of the image
            draw_line(img, line, 1/3)

        cv2.imshow("Press 'q' to close the window", img)
        key = cv2.waitKey(FRAME_DELAY_MS)
        if key == ord('q'):
            break

    cv2.destroyAllWindows()


def draw_line(img, line, scale=1.0):
    last_point = None
    for i in map(lambda x: 2*x, range(0, len(line)//2)):
        pos_x = int(line[i]*scale)
        pos_y = int(line[i + 1]*scale)
        # cv2.circle(img, (pos_x, pos_y), 1, (255, min(255, i*4), 0), 3)
        if last_point is not None:
            cv2.line(img, last_point, (pos_x, pos_y), (255, min(255, i*4), 0), 2)
        last_point = (pos_x, pos_y)
    # print(line)


def gen_frames(dir_list):
    # common = path.commonpath(dir_list)
    for dir in dir_list:
        images = list(filter(
            lambda f: path.isfile(f) and f.endswith(".jpg"),
            map(lambda d: path.join(dir, d), os.listdir(dir))
        ))
        images.sort()
        lines = list(filter(
            lambda f: path.isfile(f) and f.endswith(".txt"),
            map(lambda d: path.join(dir, d), os.listdir(dir))
        ))
        lines.sort()

        for img_filename, line_filename in zip(images, lines):
            img = cv2.imread(img_filename, cv2.IMREAD_COLOR)
            lines = read_lines(line_filename)
            yield (img, lines)


def read_lines(filename):
    lines = []
    with open(filename, "r") as f:
        for line in f.readlines():
            values = list(map(
                float,
                filter(
                    lambda x: not str.isspace(x),
                    line.split(" ")
                )
            ))
            lines.append(values)

    return lines


if __name__ == "__main__":
    parser = argparse.ArgumentParser("View the CuLane dataset")

    parser.add_argument("input_dir", type=str,
                        help="The directory containing the input images")

    options = parser.parse_args()

    main(options)
